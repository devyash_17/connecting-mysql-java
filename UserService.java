package com.example.accessingdatamysql;

import com.example.accessingdatamysql.User;
import java.lang.*;
import java.io.*;
import java.util.*;

public interface UserService {
    public Iterable<User> getAllUsers();
    public String addNewUser(Map<String, String> body);
    public String emailToName(String email);
    public String deleteUser(int id);
}