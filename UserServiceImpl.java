package com.example.accessingdatamysql;

import com.example.accessingdatamysql.User;
import com.example.accessingdatamysql.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Service;
import java.lang.*;
import java.io.*;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    boolean validate(String email)
    {
        String t = "@gmail.com";
        int len = email.length();
        int tlen = t.length();
        if(len <= tlen) return false;
        for(int i=tlen-1;i>=0;i--)
            if(t.charAt(i)!=email.charAt(i+len-tlen)) return false;
        for(int i=0;i<len-tlen;i++)
            if((email.charAt(i)>='a' && email.charAt(i)<='z') || (email.charAt(i)>='A' && email.charAt(i)<='Z'));
            else return false;
        return true;
    }

    @Override
    public Iterable<User> getAllUsers()
    {
        return userRepository.findAll();
    }

    @Override
    public String addNewUser(Map<String, String> body)
    {
        User n = new User();
        if(!validate(body.get("email"))) return "INVALID EMAIL";
        n.setName(body.get("name"));
        n.setEmail(body.get("email"));
        userRepository.save(n);
        return "Saved";
    }

    @Override
    public String emailToName(String email)
    {
        if(!validate(email)) return "INVALID EMAIL";
        User n = userRepository.findByEmail(email);
        String name = n.getName(),rname = "";
        int len = name.length();
        for(int i = 0;i < len;i++)
            rname = name.charAt(i) + rname;
        return rname;
    }

    @Override
    public String deleteUser(int id)
    {
        userRepository.deleteById(id);
        return "Deleted";
    }
}